<?php


namespace App\ExchangeApiClient\Service\Calculator\AmountCalculator;


class BidCalculator implements Calculator
{
    public function calculate(float $amount, float $exchangeRate)
    {
        return $amount * $exchangeRate;
    }
}