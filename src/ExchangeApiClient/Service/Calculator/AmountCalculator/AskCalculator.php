<?php


namespace App\ExchangeApiClient\Service\Calculator\AmountCalculator;


class AskCalculator implements Calculator
{
    public function calculate(float $amount, $exchangeRate)
    {
        return $amount / $exchangeRate;
    }
}