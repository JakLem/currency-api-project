<?php


namespace App\ExchangeApiClient\Service\Calculator\AmountCalculator;


interface Calculator
{
    public function calculate(float $amount, float $exchangeRate);
}