<?php


namespace App\ExchangeApiClient\Service\Factory;


use App\ExchangeApiClient\Dto\CalculateCurrency\CalculateCurrencyAmountRequirements;
use App\Shared\Dto\RequestBodyDto;

class CalculateCurrencyAmountRequirementsFactory
{

    public function build(RequestBodyDto $bodyDto) : CalculateCurrencyAmountRequirements
    {
        $reqs = new CalculateCurrencyAmountRequirements(
            $bodyDto->currencyFromType,
            $bodyDto->currencyToType,
            $bodyDto->amountToTransform
        );


        return $reqs;
    }
}