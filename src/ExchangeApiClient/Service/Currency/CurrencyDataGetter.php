<?php


namespace App\ExchangeApiClient\Service\Currency;


use App\ExchangeApiClient\Dto\CalculateCurrency\CalculateCurrencyAmountRequirements;
use App\NbpExchangeApiConnector\Api\CompleteCurrencyDataView;
use App\NbpExchangeApiConnector\Dto\Currency\CurrencyDataView;

class CurrencyDataGetter
{
    /**
     * @var CompleteCurrencyDataView
     */
    private CompleteCurrencyDataView $completeCurrencyDataView;

    public function __construct(CompleteCurrencyDataView $completeCurrencyDataView)
    {
        $this->completeCurrencyDataView = $completeCurrencyDataView;
    }

    public function getDataByAmountRequirements(CalculateCurrencyAmountRequirements $calculateRequirements) : CurrencyDataView
    {
        if (($currencySymbol = $calculateRequirements->currencyToType) == CalculateCurrencyAmountRequirements::MAIN_CURRENCY_TYPE) {
            $currencySymbol = $calculateRequirements->currencyFromType;
        }

        $currencyData = $this->completeCurrencyDataView->getDataByCurrencySymbol($currencySymbol);

        return $currencyData;
    }
}