<?php


namespace App\ExchangeApiClient\Service\Strategy;


use App\ExchangeApiClient\Dto\CalculateCurrency\CalculateCurrencyAmountRequirements;
use App\ExchangeApiClient\Service\Calculator\AmountCalculator\AskCalculator;
use App\ExchangeApiClient\Service\Calculator\AmountCalculator\BidCalculator;
use App\ExchangeApiClient\Service\Calculator\AmountCalculator\Calculator;

class CalculateStrategy
{

    public function selectCalculate(CalculateCurrencyAmountRequirements $amountRequirements) : Calculator
    {
        $mainCurrencyType = CalculateCurrencyAmountRequirements::MAIN_CURRENCY_TYPE;

        switch ($amountRequirements->currencyFromType) {
            case $mainCurrencyType :
                $calculator = new AskCalculator();
                break;
            default:
                $calculator = new BidCalculator();
                break;
        }

        return $calculator;
    }
}