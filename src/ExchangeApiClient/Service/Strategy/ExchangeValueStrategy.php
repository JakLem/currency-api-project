<?php


namespace App\ExchangeApiClient\Service\Strategy;


use App\ExchangeApiClient\Dto\CalculateCurrency\CalculateCurrencyAmountRequirements;
use App\NbpExchangeApiConnector\Dto\Currency\CurrencyRateView;

class ExchangeValueStrategy
{
    public function selectExchangeValue(CalculateCurrencyAmountRequirements $requirements, CurrencyRateView $rateView) : float
    {
        $mainCurrencyType = CalculateCurrencyAmountRequirements::MAIN_CURRENCY_TYPE;

        switch ($requirements->currencyFromType) {
            case $mainCurrencyType :
                $value = $rateView->ask;
                break;
            default:
                $value = $rateView->bid;
                break;
        }

        return $value;
    }
}