<?php


namespace App\ExchangeApiClient\Controller;

use App\ExchangeApiClient\Api\CompleteAmountTransform;
use App\ExchangeApiClient\Service\Factory\CalculateCurrencyAmountRequirementsFactory;
use App\ExchangeApiClient\Service\Strategy\CalculateStrategy;
use App\Shared\Service\Factory\Request\RequestBodyFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AmountExchangeController extends AbstractController
{
    /**
     * @param CompleteAmountTransform $amountTransform
     * @param Request $request
     * @param RequestBodyFactory $bodyFactory
     * @return JsonResponse
     * @throws \Exception
     */
    public function exchangeAmount(
        CompleteAmountTransform $amountTransform,
        Request $request,
        RequestBodyFactory $bodyFactory
    ) : JsonResponse
    {
        $body = $bodyFactory->createObject($request);
        $amountInNewCurrency = $amountTransform->amountTransform($body);

        return new JsonResponse($amountInNewCurrency);
    }
}