<?php


namespace App\ExchangeApiClient\Api;


use App\ExchangeApiClient\Service\Currency\CurrencyDataGetter;
use App\ExchangeApiClient\Service\Factory\CalculateCurrencyAmountRequirementsFactory;
use App\ExchangeApiClient\Service\Strategy\CalculateStrategy;
use App\ExchangeApiClient\Service\Strategy\ExchangeValueStrategy;
use App\NbpExchangeApiConnector\Api\CompleteCurrencyDataView;
use App\Shared\Dto\RequestBodyDto;

class CompleteAmountTransform
{
    /**
     * @var CalculateCurrencyAmountRequirementsFactory
     */
    private CalculateCurrencyAmountRequirementsFactory $requirementsFactory;
    /**
     * @var CalculateStrategy
     */
    private CalculateStrategy $calculateStrategy;
    /**
     * @var CurrencyDataGetter
     */
    private CurrencyDataGetter $currencyDataGetter;
    /**
     * @var ExchangeValueStrategy
     */
    private ExchangeValueStrategy $exchangeValueStrategy;

    public function __construct(
        CalculateCurrencyAmountRequirementsFactory $requirementsFactory,
        CalculateStrategy $calculateStrategy,
        CurrencyDataGetter $currencyDataGetter,
        ExchangeValueStrategy $exchangeValueStrategy
    )
    {
        $this->requirementsFactory = $requirementsFactory;
        $this->calculateStrategy = $calculateStrategy;
        $this->currencyDataGetter = $currencyDataGetter;
        $this->exchangeValueStrategy = $exchangeValueStrategy;
    }

    public function amountTransform(RequestBodyDto $bodyDto)
    {
        $calculateRequirements = $this->requirementsFactory->build($bodyDto);

        $currencyData = $this->currencyDataGetter->getDataByAmountRequirements($calculateRequirements);
        $calculator = $this->calculateStrategy->selectCalculate($calculateRequirements);
        $exchangeValue = $this->exchangeValueStrategy->selectExchangeValue($calculateRequirements, $currencyData->rate);

        $amount = $calculator
            ->calculate($calculateRequirements->amountToTransform, $exchangeValue);

        return round($amount, 2);
    }
}