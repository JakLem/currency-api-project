<?php


namespace App\ExchangeApiClient\Dto\CalculateCurrency;

class CalculateCurrencyAmountRequirements
{
    const MAIN_CURRENCY_TYPE = "PLN";

    public string $currencyFromType;
    public string $currencyToType;
    public float $amountToTransform;

    public function __construct(
        string $currencyFromType,
        string $currencyToType,
        float $amountToTransform
    )
    {
        $this->currencyFromType = $currencyFromType;
        $this->currencyToType = $currencyToType;
        $this->amountToTransform = $amountToTransform;
    }
}