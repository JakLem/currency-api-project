<?php


namespace App\ExchangeApiClient\Dto\CalculateCurrency;


class AmountExchangeInformation
{
    public CalculateCurrencyAmountRequirements $amountRequirements;
    public float $bid;
    public float $ask;

    public function __construct()
    {
    }
}