<?php


namespace App\Shared\Dto;


use Exception;

class RequestBodyDto implements \Iterator
{
    private array $field = array();
    private array $fieldKeys = array();
    private int $position;
    
    public function __get($name)
    {
        if(isset($this->field[$name]))
            return $this->field[$name];
        else
            throw new Exception("Property $name doesnt exist in request body.");
    }
    
    public function __set(string $name, $value)
    {
        $this->field[$name] = $value;
        $this->fieldKeys[] = $name;
    }
    
    public function __construct()
    {
        $this->position = 0;
    }
    
    public function rewind()
    {
        $this->position = 0;
    }
    
    public function current()
    {
        $key = $this->getPropertyName();
        return $this->field[$key];
    }
    
    public function key(): ?string
    {
        return $this->getPropertyName();
    }
    
    public function next()
    {
        ++$this->position;
    }
    
    public function valid(): bool
    {
        $key = $this->getPropertyName();
        
        // first check if key isn't null
        // method getPropertyName will return Null
        // if key doesnt exist in array fieldKeys
        return $key !== null && isset($this->field[$key]);
    }
    
    private function getPropertyName() : ?string
    {
        // checking if key on position X exist to avoid problem
        // with not existing key in array exception
        if (!isset($this->fieldKeys[$this->position])) {
            return null;
        }
        return $this->fieldKeys[$this->position];
    }
    
    public function getAsArray(): array
    {
        return (array)$this->field;
    }
}