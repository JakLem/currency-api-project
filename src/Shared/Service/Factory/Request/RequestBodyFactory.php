<?php


namespace App\Shared\Service\Factory\Request;


use App\Shared\Dto\RequestBodyDto;
use Symfony\Component\HttpFoundation\Request;

class RequestBodyFactory
{
    public function createObject(Request $request) : RequestBodyDto
    {
        $requestBodyDto = new RequestBodyDto();
        
        $body = json_decode($request->getContent(), true);
        
        if (empty($body)) {
            throw new \Exception("Request body is empty.");
        }
        
        foreach ($body as $name => $value) {
            $requestBodyDto->{$name} = $value;
        }
        
        return $requestBodyDto;
    }
    
    
    public function createArray(Request $request) : array
    {
        $body = json_decode($request->getContent(), true);
        
        if (empty($body)) {
            throw new \Exception("Request body is empty.");
        }
        
        return $body;
    }
}