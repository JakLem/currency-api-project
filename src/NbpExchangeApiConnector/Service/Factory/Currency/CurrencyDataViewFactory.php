<?php


namespace App\NbpExchangeApiConnector\Service\Factory\Currency;

use App\NbpExchangeApiConnector\Dto\Currency\CurrencyDataView;

class CurrencyDataViewFactory
{
    public function build(\stdClass $currencyData): CurrencyDataView
    {
        $rate = CurrencyRateViewFactory::build($currencyData->rates[0]);

        $currencyDataView = new CurrencyDataView(
            $currencyData->table,
            $currencyData->currency,
            $currencyData->code,
            $rate
        );

        return $currencyDataView;
    }
}