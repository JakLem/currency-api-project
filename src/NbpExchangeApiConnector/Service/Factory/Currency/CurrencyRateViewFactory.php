<?php


namespace App\NbpExchangeApiConnector\Service\Factory\Currency;


use App\NbpExchangeApiConnector\Dto\Currency\CurrencyRateView;

class CurrencyRateViewFactory
{
    private function __construct()
    {
    }


    static public function build(\stdClass $currencyRateData) : CurrencyRateView
    {
        $effectiveDate = new \DateTime($currencyRateData->effectiveDate);

        $rateView = new CurrencyRateView(
            $currencyRateData->no,
            $effectiveDate,
            $currencyRateData->bid,
            $currencyRateData->ask
        );


        return $rateView;
    }
}