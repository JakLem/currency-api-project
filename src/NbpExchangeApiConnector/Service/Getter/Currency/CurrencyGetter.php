<?php


namespace App\NbpExchangeApiConnector\Service\Getter\Currency;


use App\NbpExchangeApiConnector\Connector\NbpExchangeRateClient;
use App\NbpExchangeApiConnector\Dto\Currency\CurrencyDataView;
use App\NbpExchangeApiConnector\Service\Factory\Currency\CurrencyDataViewFactory;

class CurrencyGetter
{
    /**
     * @var NbpExchangeRateClient
     */
    private NbpExchangeRateClient $exchangeRateClient;
    /**
     * @var CurrencyDataViewFactory
     */
    private CurrencyDataViewFactory $viewFactory;

    public function __construct(
        NbpExchangeRateClient $exchangeRate,
        CurrencyDataViewFactory $viewFactory
    )
    {
        $this->exchangeRateClient = $exchangeRate;
        $this->viewFactory = $viewFactory;
    }

    public function getCurrencyData(string $currencySymbol): CurrencyDataView
    {
        $currency = $this->exchangeRateClient->getCurrencyData($currencySymbol);
        $currencyDataView = $this->viewFactory->build($currency);

        return $currencyDataView;
    }
}