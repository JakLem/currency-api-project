<?php


namespace App\NbpExchangeApiConnector\Connector;

use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NbpExchangeRateClient
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var string
     */
    private $apiHost;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->apiHost = 'http://api.nbp.pl';
    }

    /**
     * Method is getting all currencies from C table.
     * @return mixed
     * @throws TransportExceptionInterface
     */
    public function getAllCurrencies() : array
    {
        $method = sprintf("%s/%s", $this->apiHost, 'api/exchangerates/tables/C/');

        $list =  $this->httpClient->request(
            "GET",
            $method
        );

        try {
            return json_decode($list->getContent());
        } catch (\Exception $e) {
            throw new \Exception("No content for this response.");
        }
    }

    public function getCurrencyData(string $symbol) : \stdClass
    {
        $method = sprintf(
            "%s/%s%s",
            $this->apiHost,
            'api/exchangerates/rates/C/',
            $symbol
        );

        $list =  $this->httpClient->request(
            "GET",
            $method
        );


        try {
            return json_decode($list->getContent());
        } catch (\Exception $e) {
            throw new \Exception("No content for this response.");
        }
    }
}