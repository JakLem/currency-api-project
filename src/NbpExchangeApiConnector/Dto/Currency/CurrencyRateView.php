<?php


namespace App\NbpExchangeApiConnector\Dto\Currency;


use Symfony\Component\Serializer\Annotation\Groups;

class CurrencyRateView
{
    public string $no;
    public \DateTimeInterface $effectiveDate;
    public float $bid;
    public float $ask;

    public function __construct(
        string $no,
        \DateTimeInterface $effectiveDate,
        float $bid,
        float $ask
    )
    {
        $this->no = $no;
        $this->effectiveDate = $effectiveDate;
        $this->bid = $bid;
        $this->ask = $ask;
    }
}