<?php


namespace App\NbpExchangeApiConnector\Dto\Currency;


class CurrencyDataView
{
    public string $table;
    public string $currency;
    public string $code;
    public CurrencyRateView $rate;

    public function __construct(
        string $table,
        string $currency,
        string $code,
        CurrencyRateView $rate
    )
    {
        $this->table = $table;
        $this->currency = $currency;
        $this->code = $code;
        $this->rate = $rate;
    }
}