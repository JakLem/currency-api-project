<?php


namespace App\NbpExchangeApiConnector\Api;

use App\NbpExchangeApiConnector\Dto\Currency\CurrencyDataView;
use App\NbpExchangeApiConnector\Service\Getter\Currency\CurrencyGetter;

class CompleteCurrencyDataView
{

    /**
     * @var CurrencyGetter
     */
    private CurrencyGetter $currencyGetter;
    /**
     * @var AllCurrenciesView
     */
    private AllCurrenciesView $currenciesView;

    /**
     * CompleteCurrencyDataView constructor.
     * @param CurrencyGetter $currencyGetter
     */
    public function __construct(
        CurrencyGetter $currencyGetter,
        AllCurrenciesView $currenciesView
    )
    {
        $this->currencyGetter = $currencyGetter;
        $this->currenciesView = $currenciesView;
    }


    /**
     * @param $currencySymbol
     * @return CurrencyDataView
     */
    public function getDataByCurrencySymbol(string $currencySymbol): CurrencyDataView
    {
        $symbols = $this->currenciesView->getSymbolList();

        if (!in_array($currencySymbol, $symbols)) {
            throw new \Exception("Currency with this symbol doesnt not exist.");
        }

        $data = $this->currencyGetter->getCurrencyData($currencySymbol);

        return $data;
    }
}