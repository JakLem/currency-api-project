<?php


namespace App\NbpExchangeApiConnector\Api;


use App\NbpExchangeApiConnector\Connector\NbpExchangeRateClient;

class AllCurrenciesView
{
    /**
     * @var NbpExchangeRateClient
     */
    private NbpExchangeRateClient $exchangeRate;

    public function __construct(NbpExchangeRateClient $exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }

    public function getListWithData()
    {
        $currencies = $this->exchangeRate->getAllCurrencies();

        return $currencies[0];
    }

    public function getSymbolList(): array
    {
        $currencies = $this->getListWithData();

        return array_map(function($rate) {
            return $rate->code;
        }, $currencies->rates);
    }
}