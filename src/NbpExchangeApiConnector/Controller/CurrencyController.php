<?php


namespace App\NbpExchangeApiConnector\Controller;


use App\NbpExchangeApiConnector\Api\AllCurrenciesView;
use App\NbpExchangeApiConnector\Api\CompleteCurrencyDataView;
use Symfony\Component\HttpFoundation\JsonResponse;

class CurrencyController
{
    /**
     * @param AllCurrenciesView $currenciesView
     * @return JsonResponse
     */
    public function getAllCurrencies(
        AllCurrenciesView $currenciesView
    ): JsonResponse
    {
        $currencies = $currenciesView->getList();

        return new JsonResponse($currencies);
    }

    /**
     * @param string $currencySymbol
     * @param CompleteCurrencyDataView $currencyDataView
     * @return JsonResponse
     */
    public function getCurrencyData(
        string $currencySymbol,
        CompleteCurrencyDataView $currencyDataView
    ): JsonResponse
    {
        $currencyDataView = $currencyDataView->getDataByCurrencySymbol($currencySymbol);

        return new JsonResponse($currencyDataView);
    }
}