Aby uruchomić aplikacje wystarczy wklepać do terminala: php -S currencyexchange:8010 -t public

Aplikacja pozwala na pozyskanie informacji takich jak:
 - nazwa waluty
 - kod waluty
 - kurs sprzedaży oraz zakupu względem złotego.

Aplikacja pozwala na obliczenie wartości złotego względem jakiejś waluty.

Sposób funkcjonowania aplikacji.
W momencie wysyłania żądania o przeliczenie wartości walut na end point -> POST /api/currencies/exchange
Aplikacja odpytuje NBP api o aktualny kurs waluty obcej.

Przykładowe ciało dla tego end pointa to :
<br/>
{<br/>
 "currencyFromType" : "PLN",<br/>
 "currencyToType" : "USD",<br/>
 "amountToTransform": 100<br/>
}